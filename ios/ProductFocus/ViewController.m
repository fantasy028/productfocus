//
//  ViewController.m
//  RNTest
//
//  Created by Fantasy on 2017/4/26.
//  Copyright © 2017年 fantasy. All rights reserved.
//

#import "ViewController.h"
#import <RCTBundleURLProvider.h>
#import <RCTRootView.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UIButton *tb = [UIButton buttonWithType:UIButtonTypeCustom];
    [tb setTitle:@"Test" forState:UIControlStateNormal];
    [tb setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [tb addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    [tb setFrame:CGRectMake(100.f, 100.f, 80.f, 40.f)];
    [self.view addSubview:tb];
}

- (void) test:(id) sender {
    
    NSURL *jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
    
//    NSURL *jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
    
    RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                        moduleName:@"ProductFocus"
                                                 initialProperties:nil
                                                     launchOptions:nil];
    
    rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
    
    UIViewController *rootViewController = [[UIViewController alloc] init];
    rootViewController.view = rootView;
    [self.navigationController pushViewController:rootViewController
                                         animated:YES];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
