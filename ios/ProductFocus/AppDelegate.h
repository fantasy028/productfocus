//
//  AppDelegate.h
//  ProductFocus
//
//  Created by Fantasy on 2017/4/27.
//  Copyright © 2017年 fantasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

